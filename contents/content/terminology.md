---
name: Terminology
---

The following are common terms you will find throughout GitLab. For clear and consistent communication, it is important to use the correct terms.

## Nouns

### Projects & Groups

#### Project

A project is where you house your files (repository), plan your work (issues), and publish your documentation (wiki).

#### Group

Groups allow you to assemble related projects together and grant members access to several projects at once. Groups can also be nested in subgroups.

#### Members

Use the term members when discussing the people who are a part of a project or a group. Don't use the term users.

#### Roles

Users have different abilities depending on the access level they have in a particular group or project. These permission levels are defined under a set of roles. These roles include administrator, owner, master, developer, reporter, and guest.

### Comments & Discussions

#### Comment

A comment is a written piece of text that users of GitLab can create. Comments have author and timestamp meta data. Comments can be added in a variety of contexts, such as issues, merge requests, and discussions.

#### Discussion

A discussion is a group of 1 or more comments. A discussion can include subdiscussions. Some discussions have the special capability of being able to be resolved. Both the comments in the discussion and the discussion itself can be resolved.

### Issues & Merge Requests

#### Issues

Issues can have endless applications. They allow you, your team, and your collaborators to share and discuss proposals before and while implementing them.

#### Merge Requests

Merge requests allow you to exchange changes you made to source code and collaborate with other people on the same project.

#### Milestones

Milestones in GitLab are a way to track issues and merge requests created to achieve a broader goal in a certain period of time. Milestones allow you to organize issues and merge requests into a cohesive group, with an optional start date and an optional due date.

#### Activity

Activity refers to any action taken by a user that results in the creation of a system note. Commenting, resolving/opening an issue, resolving/opening a merge request, and creating/deleting a branch are all considered to be an activity.

## Verbs and adjectives

When using verbs or adjectives:

If the context clearly refers to the object, use them alone.
* **Example:** Edit or Closed

If the context isn’t clear enough, use them with the object.
* **Example:** Edit issue or Closed issues

Destruction buttons should be clear and always say what they are destroying.
* **Example:** Delete page instead of just Delete

Todo: Add comprehensive list of terminology Do/Don't
